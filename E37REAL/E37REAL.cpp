// E37REAL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using std::vector;
using std::string;
using std::stoi;
using std::to_string;
using std::cout;
using std::endl;

vector<bool> getSieve() {
	vector<bool> sieve(1000000, true);
	sieve[0] = sieve[1] = false; // 0 and 1 are not prime
	unsigned int i = 2;
	while (i <= 1000) {
		if (sieve[i]) {
			for (unsigned int j = i * 2; j < sieve.size(); j += i) {
				sieve[j] = false;
			}
		}
		++i;
	}
	return sieve;
}

vector<unsigned int> get_truncs(unsigned int num) {
	vector<unsigned int> vec;
	string snum = to_string(num);
	unsigned int len = snum.size();
	vec.reserve(len * 2 - 1);

	vec.push_back(num);
	if (num < 10) {
		return vec;
	}
	for (unsigned int i = 1; i < len; ++i) {
		string ssub = snum.substr(0, i);
		unsigned int sub = stoi(ssub);
		vec.push_back(sub);
	}
	unsigned int l = len - 1;
	unsigned int r = 1;
	while (l != 0) {
		string ssub = snum.substr(l, r);
		unsigned int sub = stoi(ssub);
		vec.push_back(sub);
		--l;
		++r;
	}
	return vec;
}

int main()
{
	unsigned int count = 0, sum = 0;
	auto sieve = getSieve();
	for (unsigned int p = 3; p < sieve.size(); p += 2) {
		if (p < 10)
			continue; // 2,3,5,7 are not considered truncable primes
		if (sieve[p]) {
			auto truncs = get_truncs(p);
			bool pgood = true;

			for (auto t : truncs) {
				if (!sieve[t]) {
					pgood = false;
					break;
				}
			}
			if (pgood) {
				++count;
				sum += p;
				cout << "FOUND:" << p << "  SUM is " << sum << "  COUNT is " << count << endl;
			}
		}
	}
    return 0;
}

